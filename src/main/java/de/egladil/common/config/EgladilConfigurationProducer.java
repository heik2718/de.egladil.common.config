//=====================================================
// Projekt: de.egladil.config
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import de.egladil.common.config.qualifiers.BQMConfiguration;
import de.egladil.common.config.qualifiers.BQPersistenceConfiguration;
import de.egladil.common.config.qualifiers.BVPersistenceConfiguration;
import de.egladil.common.config.qualifiers.CryptoConfiguration;
import de.egladil.common.config.qualifiers.EmailConfiguration;
import de.egladil.common.config.qualifiers.MKMPersistenceConfiguration;
import de.egladil.common.config.qualifiers.MKVPersistenceConfiguration;
import de.egladil.common.config.qualifiers.QuizPersistenceConfiguration;

/**
 * PersistenceConfigurationProducer
 */
@ApplicationScoped
public class EgladilConfigurationProducer {

	@Produces
	@ApplicationScoped
	@CryptoConfiguration
	public IEgladilConfiguration createCryptoConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "heike";
			}

		};
		return persistenceConfiguration;
	}

	@Produces
	@ApplicationScoped
	@EmailConfiguration
	public IEgladilConfiguration createEmailConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "email.properties";
			}

		};
		return persistenceConfiguration;
	}

	@Produces
	@ApplicationScoped
	@BQPersistenceConfiguration
	public IEgladilConfiguration createBQPersistenceConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bq_persistence.properties";
			}

		};
		return persistenceConfiguration;
	}

	@Produces
	@ApplicationScoped
	@BVPersistenceConfiguration
	public IEgladilConfiguration createBVPersistenceConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bv_persistence.properties";
			}

		};
		return persistenceConfiguration;
	}

	@Produces
	@ApplicationScoped
	@BQMConfiguration
	public IEgladilConfiguration createBlitzquizManufakturConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bqm_service.properties";
			}

		};
		return persistenceConfiguration;
	}

	@Produces
	@ApplicationScoped
	@MKMPersistenceConfiguration
	public IEgladilConfiguration createMinikaenguruPersistenceConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "mkm_persistence.properties";
			}
		};
		return persistenceConfiguration;
	}

	@Produces
	@ApplicationScoped
	@MKVPersistenceConfiguration
	public IEgladilConfiguration createMKVPersistenceConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "mkv_persistence.properties";
			}

		};
		return persistenceConfiguration;
	}

	@Produces
	@ApplicationScoped
	@QuizPersistenceConfiguration
	public IEgladilConfiguration createQuizPersistenceConfiguration() {
		final IEgladilConfiguration persistenceConfiguration = new AbstractEgladilConfiguration() {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "quiz_persistence.properties";
			}

		};
		return persistenceConfiguration;
	}

}
