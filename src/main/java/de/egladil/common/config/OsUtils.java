//=====================================================
// Projekt: de.egladil.common.config
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.config;

/**
 * OsUtils
 */
public final class OsUtils {

	/**
	 * Gibt dem Pfad zum Dev-Config-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevConfigRoot() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/.esapi/mkvapi/config";
		}
		return "/home/heike/git/konfigurationen/mkverwaltung/configAll";
	}

	/**
	 * Gibt dem Pfad zum Dev-Config-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getDevConfigRootWithInvalidMail() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/.esapi/mkvapi/config";
		}
		return "/home/heike/git/konfigurationen/mkverwaltung/configInvalid";
	}

	/**
	 * Gibt dem Pfad zum Dev-Config-Verzeichnis zurück.<br>
	 * <br>
	 * <b>Achtung: </b>Nur in Tests verwenden!!!!
	 *
	 * @return
	 */
	public static String getQSConfigRoot() {
		final String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			return "C:/Users/Winkelv/.esapi/mkvapi/config";
		}
		return "/home/heike/deploy/test/MKV-V1.6-QS/mkvapi/config";
	}

}
