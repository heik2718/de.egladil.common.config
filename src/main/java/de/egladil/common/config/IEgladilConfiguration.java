package de.egladil.common.config;

import java.util.Map;

public interface IEgladilConfiguration {

	Map<String, String> getConfigurationMap();

	/**
	 *
	 * @param key
	 * @return Integer oder den default-Wert, wenn die
	 */
	Integer getIntegerProperty(String key);

	/**
	 *
	 * @param key
	 * @return Integer oder den default-Wert, wenn die
	 */
	Long getLongProperty(String key);

	/**
	 * Secrets werden nur als char[] herausgegeben, da sie besser gegen HeapDump-Ausgaben geschützt sind.
	 *
	 * @param key
	 * @return char[]
	 */
	char[] getSecretProperty(String key) throws EgladilConfigurationException;

	String getProperty(String key);

	/**
	 * Zu Testzwecken.
	 */
	void printConfiguration();

	/**
	 * Initialisiert die Properties mittels der in pathConfigRoot liegenden Datei.
	 *
	 * @param pathConfigRoot
	 */
	void init(String pathConfigRoot) throws EgladilConfigurationException;
}