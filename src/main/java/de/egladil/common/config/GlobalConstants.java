//=====================================================
// Projekt: de.egladil.constants
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.config;

/**
 * GlobalConstants
 */
public interface GlobalConstants {

	String LOG_PREFIX_DATENMUELL = "Datenmuell bleibt: ";

	String LOG_PREFIX_MAILVERSAND = "Mailversand: ";

	String LOG_PREFIX_IMPOSSIBLE = "Unmoeglicher Programmzweig: ";

	String LOG_PREFIX_DIRECTORY_INJECTION = "Directory Injection Attempt: ";

	String LOG_PREFIX_BOT = "Possible BOT Attack: ";

	String LOG_PREFIX_DOWNLOAD = "Download: ";

	String LOG_PREFIX_UPLOAD = "Upload: ";

	String LOG_PREFIX_MAILQUEUE = "Mailqueue: ";

	boolean TEST = false;

}
