//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.config;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalUnit;
import java.util.Date;

/**
 * TimeUtils
 */
public class TimeUtils {

	/**
	 * Addiert zu jetzt die angegebene Anzahl Zeiteinheiten.
	 *
	 * @param jetzt
	 * @param expireIn
	 * @param timeUnit
	 * @return
	 */
	public static Date calculateExpireTime(Date jetzt, int expireIn, TemporalUnit unit) {
		LocalDateTime later = LocalDateTime.ofInstant(jetzt.toInstant(), ZoneId.systemDefault()).plus(expireIn, unit);
		ZonedDateTime zdt = later.atZone(ZoneId.systemDefault());
		Date result = Date.from(zdt.toInstant());
		return result;
	}
}
