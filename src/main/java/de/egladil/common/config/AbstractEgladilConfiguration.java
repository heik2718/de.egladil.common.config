//=====================================================
// Projekt: de.egladil.config
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * AbstractEgladilConfiguration
 */
public abstract class AbstractEgladilConfiguration implements IEgladilConfiguration, Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(AbstractEgladilConfiguration.class);

	protected String pathConfigRoot;

	private Properties properties = new Properties();

	/**
	 * Erzeugt eine Instanz von AbstractEgladilConfiguration
	 */
	public AbstractEgladilConfiguration() {
		// init();
	}

	public AbstractEgladilConfiguration(final String pathConfigRoot) {
		init(pathConfigRoot);
	}

	/**
	 *
	 * @see de.egladil.common.config.IEgladilConfiguration#init(java.lang.String)
	 */
	@Override
	public void init(final String pathConfigRoot) throws EgladilConfigurationException {
		LOG.info(getClass().getSimpleName() + ": [pathConfigRoot=" + pathConfigRoot + "]");
		final String pathSecret = pathConfigRoot + File.separator + getConfigFileName();
		InputStream in = null;
		try {
			LOG.debug("lesen Konfiguration aus " + pathSecret);
			in = new FileInputStream(pathSecret);
			properties.load(in);
			LOG.debug("Konfiguration aus " + pathSecret + " gelesen");
		} catch (final IOException e) {
			final String msg = "Pfad zur Konfigurationsdatei wurde nicht gefunden: die Umgebungsvariable EGLADIL_CONFIG_HOME existiert nicht oder zeigt nicht auf das richtige Verzeichnis oder das Verzeichnis enthält keine Datei namens "
				+ getConfigFileName() + ".";
			LOG.error(msg);
			throw new EgladilConfigurationException(e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * @return
	 */
	protected abstract String getConfigFileName();

	/**
	 * @see de.egladil.common.config.IEgladilConfiguration#getConfigurationMap()
	 */
	@Override
	public Map<String, String> getConfigurationMap() {
		final Map<String, String> result = new HashMap<>();
		final Set<Entry<Object, Object>> entrySet = properties.entrySet();
		for (final Entry<Object, Object> entry : entrySet) {
			final String key = (String) entry.getKey();
			final String value = (String) entry.getValue();
			result.put(key, value);
		}
		return result;
	}

	/**
	 * @see de.egladil.common.config.IEgladilConfiguration#getIntegerProperty(java.lang.String)
	 */
	@Override
	public Integer getIntegerProperty(final String key) {
		final String prop = this.getProperty(key);
		try {
			return Integer.parseInt(prop.toString());
		} catch (final NumberFormatException e) {
			throw new EgladilConfigurationException("Property [" + key + "] in " + getConfigFileName() + " ist nicht numerisch");
		}
	}

	/**
	 * @see de.egladil.common.config.IEgladilConfiguration#getLongProperty(java.lang.String)
	 */
	@Override
	public Long getLongProperty(final String key) {
		final String prop = this.getProperty(key);
		try {
			return Long.parseLong(prop.toString());
		} catch (final NumberFormatException e) {
			throw new EgladilConfigurationException("Property [" + key + "] in " + getConfigFileName() + " ist nicht numerisch");
		}
	}

	/**
	 * @see de.egladil.common.config.IEgladilConfiguration#getSecretProperty(java.lang.String)
	 */
	@Override
	public char[] getSecretProperty(final String key) throws EgladilConfigurationException {
		Object prop = this.properties.get(key);
		if (prop == null) {
			throw new EgladilConfigurationException("Property [" + key + "] fehlt in " + getConfigFileName());
		}
		final char[] result = ((String) prop).toCharArray();
		prop = null;
		return result;
	}

	/**
	 * @see de.egladil.common.config.IEgladilConfiguration#getProperty(java.lang.String)
	 */
	@Override
	public String getProperty(final String key) {
		final Object prop = this.properties.get(key);
		if (prop == null) {
			throw new EgladilConfigurationException("Property [" + key + "] fehlt in " + getConfigFileName());
		}
		return prop.toString();
	}

	@Override
	public void printConfiguration() {
		final Map<String, String> map = getConfigurationMap();
		LOG.info("Konfiguration: {}", getConfigFileName());
		System.out.println("Konfiguration " + pathConfigRoot + "/" + getConfigFileName());
		for (final String key : map.keySet()) {
			LOG.info(key + "=" + map.get(key));
			System.out.println(key + "=" + map.get(key));
		}
	}
}
