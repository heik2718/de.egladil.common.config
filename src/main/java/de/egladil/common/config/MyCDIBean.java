//=====================================================
// Projekt: de.egladil.email.storage
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.config;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.config.qualifiers.BQPersistenceConfiguration;
import de.egladil.common.config.qualifiers.EmailConfiguration;

/**
 * MyCDIBean
 */
public class MyCDIBean {

	private static final Logger LOG = LoggerFactory.getLogger(MyCDIBean.class);

	@Inject
	@EmailConfiguration
	private IEgladilConfiguration emailConfiguration;

	@Inject
	@BQPersistenceConfiguration
	private IEgladilConfiguration bqConfiguration;

	/**
	 * Erzeugt eine Instanz von MyCDIBean
	 */
	public MyCDIBean() {
		LOG.debug("construct MyCDIBean");
	}

	public void printConfigurations() {
		LOG.debug("bqConfiguration:");
		bqConfiguration.printConfiguration();
		LOG.debug("emailConfiguration:");
		emailConfiguration.printConfiguration();
	}

}
