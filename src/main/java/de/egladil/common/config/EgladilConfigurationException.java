//=====================================================
// Projekt: de.egladil.exceptions
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.config;

/**
 * @author heike
 *
 */
public class EgladilConfigurationException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public EgladilConfigurationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public EgladilConfigurationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public EgladilConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EgladilConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
