//=====================================================
// Projekt: de.egladil.common.config
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.config;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import de.egladil.common.config.TimeUtils;

/**
 * TimeUnitTest
 */
public class TimeUtilsTest {

	@Test
	@DisplayName("calculateExpireTime returns correct value with minutes")
	public void calculateExpireTime1() throws Exception {
		// Arrange
		final String now = "12.12.2016 10:05:07";
		final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

		final String expectedDateString = "12.12.2016 10:20:07";
		final Date jetzt = sdf.parse(now);
		final int anzahl = 15;
		final TemporalUnit unit = ChronoUnit.MINUTES;

		// Act
		final Date result = TimeUtils.calculateExpireTime(jetzt, anzahl, unit);

		// Assert
		final String actualDateString = sdf.format(result);
		assertEquals(expectedDateString, actualDateString);
	}

	@Test
	@DisplayName("calculateExpireTime returns correct value with days")
	public void calculateExpireTime2() throws Exception {
		// Arrange
		final String now = "12.12.2016 10:05:07";
		final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

		final String expectedDateString = "14.12.2016 10:05:07";
		final Date jetzt = sdf.parse(now);
		final int anzahl = 2;
		final TemporalUnit unit = ChronoUnit.DAYS;

		// Act
		final Date result = TimeUtils.calculateExpireTime(jetzt, anzahl, unit);

		// Assert
		final String actualDateString = sdf.format(result);
		assertEquals(expectedDateString, actualDateString);
	}

}
