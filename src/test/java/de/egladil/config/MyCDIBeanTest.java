//=====================================================
// Projekt: de.egladil.email.storage
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.config;

import javax.inject.Inject;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.config.MyCDIBean;

/**
 * MyCDIBeanTest
 */
public class MyCDIBeanTest {

	private static Logger LOG = LoggerFactory.getLogger(MyCDIBean.class);

	@Inject
	private MyCDIBean cdiBean;

	private Weld weld;

	@BeforeEach
	public void setUp() {
		weld = new Weld();
		final WeldContainer weldContainer = weld.initialize();
		cdiBean = weldContainer.instance().select(MyCDIBean.class).get();
		LOG.debug("weld initialized");
	}

	@AfterEach
	public void teardownClass() {
		weld.shutdown();
		LOG.debug("weld cancelled");
	}

	@Test
	public void should_print_the_configuration() {
		cdiBean.printConfigurations();
	}

}
